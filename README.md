# omeka2elk

**omeka2elk** is a set of scripts intended to grab all "Procès-verbaux du Bureau des longitudes" hosted on http://bdl.ahp-numerique.fr/ and stored them in a local [Elasticsearch](https://www.elastic.co/) database.
BdL website, based on an [omeka engine](https://omeka.org/), is not convenient nor performant enough to analyze data hosted. Therefore, the aim of this project is to transfer the data to another database engine, Elasticsearch, and use [Kibana](https://www.elastic.co/kibana) portal to be able to make advanced searches.
This project is very specific to BdL website, both on website structure and data structure, and could not be easily reused for another website based on omeka.
Also, part of this project is not relevant if you can directly contact the webmaster of the website, who would probably be able to extract data more efficiently. 



# Prerequisites

These scripts have been tested with Elasticsearch and Kibana both version 7.17.6 and with Python 3.9.13, using following packages:
* elasticsearch	version 7.17.6 (Python package to connect to Elasticsearch cluster must be in the same version as the cluster itself) 
* beautifulsoup4 	version 4.11.1
* requests		version 2.28.1
* python-slugify	version 6.1.2

## Side note
You can install locally Elasticsearch and Kibana locally using Docker containers.
```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.17.6
docker pull docker.elastic.co/kibana/kibana:7.17.6

docker network create elastic
docker run --name es01-test --net elastic -p 127.0.0.1:9200:9200 -p 127.0.0.1:9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.17.6
docker run --name kib01-test --net elastic -p 127.0.0.1:5601:5601 -e "ELASTICSEARCH_HOSTS=http://es01-test:9200" docker.elastic.co/kibana/kibana:7.17.6
```
To access Kibana, go to http://localhost:5601.

# download_from_bdl_website.py

This script will navigate through all BdL website pages related to "procès verbaux", parse them, and save them as json files.
Json files will be saved in folder `./data` and will be named with the "Title" of the document.
For instance, page http://bdl.ahp-numerique.fr/items/show/833 will be saved as `./data/833_seance-du-10-fevrier-1808.json`.
Logs are stored in log file `./log/download_from_bdl_website_<YYYYMMDD>.log`

# upload_to_elastic.py
This script will read all json files stored in folder `./data` parse and enrich them, and send them in Elasticsearch cluster, into index named pv.
Logs are stored in log file `./log/upload_to_elastic_<YYYYMMDD>.log`
Enrichment will consist in:
* adding an attribute to 'Type' item : "Copie" if Collection attribute contains the word "copie", "Original" otherwise ; this will help to filter efficicently copies registries.
* items 'Type', 'Éditeur', 'Format', 'Contributeur' will be converted into lists, asumming that values are separated by a semi-column.
* if item 'Date', contains multiple values, only the first one will be kept.
* a new item 'Présents' is created, listing all the persons listed in the section "Etaient présents" of the 'Description item ; warning, this item may be empty since the section "Etaient présents" is not always existing

# create_kibana_objects.py
This script will create some basic objects in Kibana so that data imported will be immediately usable.

Objects created:
* an **Index Pattern** based on `pv` index
* a **Search** based on this index pattern
* `Time filter defaults` setting set with the interval 1795-01-01 to 1933-01-01

You can bypass this script if you are familiar enough with Kibana and want to customise your workspace.

# Usage
1. make sure Elasticsearch and Kibana are running
2. run script `import_from_bdl_website.py` (a couple of hours)
3. run script `upload_to_elastic.py` (a couple of minutes)
4. run script `create_kibana_objects.py` (a couple of seconds)
5. go to http://localhost:5601/app/discover and browse the dataset

# License
[GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html)
