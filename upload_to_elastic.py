import json
import re
import os
from elasticsearch import Elasticsearch
import logging
from datetime import datetime

# set some variables
DATA_DIRECTORY = os.path.join(os.getcwd(), 'data')
LOG_DIRECTORY = os.path.join(os.getcwd(), 'log')
LOG_LEVEL = 'INFO'
ES_HOST = 'localhost'
ES_PORT = 9200
ES_INDEX = 'pv'

# create logger
log_file = "upload_to_elastic_%s.log" % (datetime.now().strftime("%Y%m%d"))
logger = logging.getLogger('simple_example')
logging.basicConfig(filename=os.path.join(LOG_DIRECTORY, log_file), format='%(asctime)s;%(levelname)s;%(message)s')
logger.setLevel(LOG_LEVEL)

# upload
logger.info("Upload - start")
es = Elasticsearch([{'host': ES_HOST, 'port': ES_PORT}])

try:
    for file_name in os.listdir(DATA_DIRECTORY):
        logger.info("Document to process: %s", file_name)
        print(file_name)

        if file_name.endswith(".json"):
            f = open(os.path.join(DATA_DIRECTORY,  file_name), encoding='utf-8')
            file_content = f.read()
            # Send the data into es
            try:
                # clean data
                data = json.loads(file_content)

                # transform some items into lists
                for item in ['Type', 'Éditeur', 'Format', 'Date']:
                    if item in data:
                        data[item] = [d.strip() for d in data[item].split(';') if len(d.strip()) > 0]

                # horrible hack #1: for Contributeur item semicolon is not a totally reliable separator
                # example: 1505_seance-du-22-nov-1820.json
                if 'Contributeur' in data:
                    data['Contributeur'] = [d.strip()+')' for d in data['Contributeur'].split(');') if len(d.strip()) > 0]

                # horrible hack #2: Date item can have multiple values, let's use only the first one
                # example: 6171_bureau-des-longitudes-proces-verbal-de-la-seance-du-21-et-du-28-aout-1912.json
                if 'Date' in data:
                    data['timestamp'] = data['Date'][0]

                # add Original/Copie value to Collection item
                if 'Collection' in data:
                    if data['Collection'].count('copie') > 0:
                        data['Type'].append('Copie')
                    else:
                        data['Type'].append('Original')

                # add Présents item
                if 'Description' in data:
                    # horrible hack #3: some of the documents contain the list of persons attending,
                    # but, the format is not always the same
                    # example with []: http://bdl.ahp-numerique.fr/items/show/1567
                    # example with <>: http://bdl.ahp-numerique.fr/items/show/7284
                    data['Présents'] = re.findall('(?i)\[.*présents.*\]', data['Description']) + \
                                       re.findall('(?i)\<.*présents.*\>', data['Description'])
                    for p in data['Présents']:
                        data['Description'] = data['Description'].replace(p, '')

                # send to es
                es.index(index=ES_INDEX, id=os.path.splitext(file_name)[0], document=data)

            except Exception as e:
                logger.error(e)
                print(e)

except Exception as e:
    logger.error(e)

finally:
    logger.info("Upload - end")
