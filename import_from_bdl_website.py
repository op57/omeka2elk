
import os
import requests
from bs4 import BeautifulSoup
import json
from slugify import slugify
import logging
from datetime import datetime

# set some variables
DATA_DIRECTORY = os.path.join(os.getcwd(), 'data')
LOG_DIRECTORY = os.path.join(os.getcwd(), 'log')
LOG_LEVEL = 'INFO'
URL_ROOT = 'http://bdl.ahp-numerique.fr'
ITEM_ID_MIN = 1
ITEM_ID_MAX = 11281

# create logger
log_file = "import_from_bdl_website_%s.log" % (datetime.now().strftime("%Y%m%d"))
logger = logging.getLogger('simple_example')
logging.basicConfig(filename=os.path.join(LOG_DIRECTORY, log_file), format='%(asctime)s;%(levelname)s;%(message)s')
logger.setLevel(LOG_LEVEL)

logger.info("Download - start")

file_list = [f for f in os.listdir(os.path.join(os.getcwd(), 'data')) if os.path.isfile(os.path.join(os.path.join(os.getcwd(), 'data'), f))]
already_processed = [int(f[:f.find('_')]) for f in file_list]
to_process = [i for i in range(ITEM_ID_MIN, ITEM_ID_MAX) if i not in already_processed]

logger.info("Documents to process: %s", len(to_process))

for i in to_process:
    try:
        logger.info("Process document %i", i)

        # get item
        url_item = URL_ROOT + "/items/show/" + str(i)
        html_item = requests.get(url_item).text
        soup_item = BeautifulSoup(html_item, 'html.parser')

        content = soup_item.find('div', attrs={'id': 'content'}).find('h1').text.strip()
        if content == 'Erreur 404 - Page non trouvée':
            print(i, 'Page not found')
            logger.warning("Document %i cannot be processed: Page not found", i)

        else:
            table = soup_item.find('div', attrs={'class': 'meta-table'})
            fields = table.find_all('tr')

            pv = dict()
            for field in fields:
                try:
                    k = field.find('td', attrs={'class': 'meta-title'}).text.strip()
                except:
                    k = field.find('td', attrs={'class': 'meta-title'})
                try:
                    v = field.find('td', attrs={'class': 'meta-value'}).text.strip()
                except:
                    v = field.find('td', attrs={'class': 'meta-value'})
                pv[k] = v
            output_file = str(i)+'_'+slugify(pv['Titre'])+'.json'
            output_file = os.path.join(DATA_DIRECTORY, output_file)
            with open(output_file, 'w', encoding='utf-8') as f:
                json.dump(pv, f, ensure_ascii=False, indent=4)

    except Exception as e:
        logger.error(e)
        print(e)
