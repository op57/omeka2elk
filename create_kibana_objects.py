import os
import requests
import logging
from datetime import datetime

# set some variables
KIBANA_URL = 'http://localhost'
KIBANA_PORT = 5601
LOG_DIRECTORY = os.path.join(os.getcwd(), 'log')
LOG_LEVEL = 'INFO'

# create logger
log_file = "create_kibana_objects_%s.log" % (datetime.now().strftime("%Y%m%d"))
logger = logging.getLogger('simple_example')
logging.basicConfig(filename=os.path.join(LOG_DIRECTORY, log_file), format='%(asctime)s;%(levelname)s;%(message)s')
logger.setLevel(LOG_LEVEL)

logger.info("Update Kibana - start")

try:
    headers = {'kbn-xsrf': 'true', 'Content-Type': 'application/json'}

    # index pattern
    logger.info("Create Index Pattern")
    url_index_pattern = 'http://localhost:5601/api/index_patterns/index_pattern'
    data_index_pattern = {'override': 'true', 'index_pattern': {'title': 'pv', 'timeFieldName': 'timestamp'}}
    response_index_pattern = requests.post(url_index_pattern, headers=headers, json=data_index_pattern)
    logger.info("Response code: %s", response_index_pattern.status_code)
    logger.info("Response: %s", response_index_pattern.json())

    # search
    logger.info("Create Search")
    url_search = 'http://localhost:5601/api/saved_objects/search'
    data_search = {'attributes':
                       {'columns': ['Titre', 'Description'],
                        'sort': [['Date', 'desc']],
                        'title': 'pv'}
                   }
    response_search = requests.post(url_search, headers=headers, json=data_search)
    logger.info("Response code: %s", response_search.status_code)
    logger.info("Response: %s", response_search.json())

    # config
    logger.info("Update config")
    data_config = {'attributes':
                       {'defaultIndex': 'pv',
                        'timepicker:timeDefaults': '{\n  \'from\': \'1795-01-01\',\n  \'to\': \'1933-01-01\'\n}'}
                   }
    url_config = 'http://localhost:5601/api/saved_objects/config'
    response_config = requests.post(url_config, headers=headers, json=data_config)
    logger.info("Response code: %s", response_config.status_code)
    logger.info("Response: %s", response_config.json())


except Exception as e:
    logger.error(e)

finally:
    logger.info("Update Kibana - end")
